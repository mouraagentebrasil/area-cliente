import Ember from 'ember';

export default Ember.Component.extend({
  actions: {
    getSelectSexo(index, sexo) {
      return '<select id="lead_agregado_sexo_save_' + index + '">' +
        '<option ' + (sexo == "m" ? "selected" : "") + '>Masculino</option>' +
        '<option ' + (sexo == "f" ? "selected" : "") + '>Femino</option>' +
        '</select>';
    },
    enableToEdit(dados, index) {
      let nome = $('#lead_agregado_nome').text()
      $('#lead_agregado_nome_' + index).html("<input id='lead_agregado_nome_save_" + index + "' class='input_c' value='" + dados.lead_agregado_nome + "'/>");
      $('#lead_agregado_sexo_' + index).html(
        '<select class="input_c" id="lead_agregado_sexo_save_' + index + '">' +
        '<option ' + (dados.lead_agregado_sexo == "m" ? "selected" : "") + '>Masculino</option>' +
        '<option ' + (dados.lead_agregado_sexo == "f" ? "selected" : "") + '>Femino</option>' +
        '</select>'
      );
      $('#lead_agregado_nascimento_' + index).html("<input id='lead_agregado_nascimento_save_" + index + "' data-mask='00/00/0000' class='input_c' value='" + dados.lead_agregado_nascimento + "'/>");
      $('#lead_agregado_mae_' + index).html("<input id='lead_agregado_mae_save_" + index + "' class='input_c' value='" + dados.lead_agregado_mae + "'/>");
      $('#saveBene_' + index).css('display', 'inline');
      $('#editBene_' + index).css('display', 'none');
    },
    saveBene(index) {
      let objectSave = {
        'lead_agregado_nome': $('#lead_agregado_nome_save_' + index).val(),
        'lead_agregado_sexo': $('#lead_agregado_sexo_save_' + index).val(),
        'lead_agregado_nascimento': $('#lead_agregado_nascimento_save_' + index).val(),
        'lead_agregado_mae': $('#lead_agregado_mae_save_' + index).val()
      }

      $('#lead_agregado_nome_' + index).html($('#lead_agregado_nome_save_' + index).val())
      $('#lead_agregado_sexo_' + index).html($('#lead_agregado_sexo_save_' + index).val())
      $('#lead_agregado_nascimento_' + index).html($('#lead_agregado_nascimento_save_' + index).val())
      $('#lead_agregado_mae_' + index).html($('#lead_agregado_mae_save_' + index).val())

      $('#saveBene_' + index).css('display', 'none');
      $('#editBene_' + index).css('display', 'inline');
      console.log(objectSave);
    },
    maskDate(index){
        $('#lead_agregado_nascimento_save_'+index).mask('00/00/0000');
    }
  }
});
