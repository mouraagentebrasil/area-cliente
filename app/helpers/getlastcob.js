export function getlastcob(data) {

  let objectReturn;
  for (let i = 0; i < data[0].length; i++) {
    if (data[0][i].situacao === false) {
      objectReturn = data[0][i];
      return objectReturn.boleto.cobranca_id;
    }
  }
}
export default Ember.Helper.helper(getlastcob);
