export function getlastcobdiasparavencer(data) {

  let objectReturn;
  if (data.boleto !== null) {
    for (let i = 0; i < data[0].length; i++) {
      if (data[0][i].situacao === false) {
        objectReturn = data[0][i];
        return objectReturn.boleto.dias_para_vencer;
      }
    }
  }
}
export default Ember.Helper.helper(getlastcobdiasparavencer);
