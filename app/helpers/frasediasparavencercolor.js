export function frasediasparavencercolor(data) {
  let objectReturn;
  if (data.boleto !== null) {
    for (let i = 0; i < data[0].length; i++) {
      if (data[0][i].situacao === false) {
        objectReturn = data[0][i];
        if(objectReturn.boleto.dias_para_vencer>1){
           return "color:#8300e9";
        } else if(objectReturn.boleto.dias_para_vencer == 0){
            return "color:#7cda24";
        } else {
            return "color:#F22613;";
        }
      }
    }
  }
}
export default Ember.Helper.helper(frasediasparavencercolor);