export function listparcelas(data) {

  let objectReturn;
  let color_avencer = {
    color: "#e09900",
    icon: "",
  };
  let color_pago = {
    color: "#7cda24",
    icon: "",
  };
  let color_vencido = {
    color: "#F22613",
    icon: "",
  };
  let html = "";
  let date = new Date();
  let object = null;
  let status = null;
  let linkBoleto = null;

  var functionGenerate = function (data) {
    let now = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    for (let i = 0; i < data[0].length; i++) {

      if (data[0][i].financeiro_forma_pagamento_id == 1) {
        if (data[0][i].foi_pago == 1) {
          object = color_pago;
          status = 'PAGO';
          linkBoleto = "#";
        } else if (data[0][i].boleto !== null && new Date(data[0][i].boleto.data_vencimento ) > new Date()) {
          object = color_avencer;
          status = 'À VENCER';
          linkBoleto = "https://pag.ag/" + data[0][i].boleto.cobranca_id;
        } else if (data[0][i].boleto !== null &&  new Date() > new Date(data[0][i].boleto.data_vencimento) && data[0][i].foi_pago == 0) {
          object = color_vencido;
          status = 'VENCIDO';
          linkBoleto = "https://pag.ag/" + data[0][i].boleto.cobranca_id;
        }
      } else {
         let data_vencimento = data[0][i].data_vencimento.split("/")[2]+"-"+data[0][i].data_vencimento.split("/")[1]+"-"+data[0][i].data_vencimento.split("/")[0];
        if (data[0][i].foi_pago == 1) {
          object = color_pago;
          status = 'PAGO';
          linkBoleto = "#";
        } else if (new Date() < new Date(data_vencimento) && data[0][i].foi_pago == 0) {
          object = color_avencer;
          status = 'À VENCER';
          linkBoleto = "#";
        } else if (new Date() > new Date(data_vencimento) && data[0][i].foi_pago == 0) {
          object = color_vencido;
          status = 'VENCIDO';
          linkBoleto = "#";
        }
      }
      //caso seja cartão de credito
      linkBoleto = data[0][i].financeiro_forma_pagamento_id == 1 ? linkBoleto : "#";
      html += '<div class="et_pb_blurb_content">';
      html += '<div class="et_pb_main_blurb_image"><a href="#"><span class="et-pb-icon et-waypoint et_pb_animation_off" style="color: ' + object.color + '; font-size:20px; position: relative; top:12px;">' + object.icon + '</span></a></div>';
      html += '<div class="et_pb_blurb_container">';
      html += '<h4><a href="' + linkBoleto + '" style="font-size: 12px;" target="_blank">' + (data[0][i].data_vencimento) + ' - ' + (status) + '</a></h4>';
      html += '</div>';
      html += '</div>';
    }
    $('#list_parcelas').html(html);
  }

  functionGenerate(data);
}
export default Ember.Helper.helper(listparcelas);
