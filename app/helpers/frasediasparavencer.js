export function frasediasparavencer(data) {
  let objectReturn;
  if (data.boleto !== null) {
    for (let i = 0; i < data[0].length; i++) {
      if (data[0][i].situacao === false) {
        objectReturn = data[0][i];
        if(objectReturn.boleto.dias_para_vencer>1){
           return "Dias para vencer sua fatura";
        } else if(objectReturn.boleto.dias_para_vencer == 0){
            return "Sua fatura vence HOJE";
        } else {
            return "Dias VENCIDA";
        }
      }
    }
  }
}
export default Ember.Helper.helper(frasediasparavencer);