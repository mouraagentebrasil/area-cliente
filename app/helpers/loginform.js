export function loginform() {
  $('#cpf').on('keyup', (e) => {
    e.preventDefault();
    let cpf = $('#cpf').val();
    if (cpf.length == 11) {
      getCPFCad(cpf).then(() => {
        if ($('#loginLead').length == 0) {
          $('.login_form').append('<a style="margin-top:20px;" href="#" id="loginLead" class="button-rounded button-rounded__orange bt-quotation">Acessar Meu Plano</a>');
          $('#lead_nome_s').remove();
          $('#lead_fone_s').remove();
          $('#lead_email_s').remove();
          $('#saveLead').remove();
        }
      }, (response) => {
        getOtherFields();
        $('#loginLead').remove();
      });
    }
  });
  $(document).on('click', '#loginLead', function () {
    let cpf = $('#cpf').val();
    if (cpf.length == 11) {
      window.location.href = 'app/' + cpf;
    } else {
      alert('CPF invalido');
    }
  });

  $(document).on('click', '#saveLead', function () {
    verificaButton();
  });

  $(document).on('keyup', '#lead_nome', function () {
    this.value = this.value.replace(/[^a-zA-Z ]/g, '');
  });

  var getCPFCad = function (cpf) {
    return Ember.$.ajax({
      'method': 'get',
      'url': 'https://client.agentebrasil.com/client/financeiro-contratacao-by-cpf/' + cpf
    });
  };

  var getOtherFields = function () {
    if ($("#lead_fone").length == 0) {
      $('.login_form').append('<p style="font-size:25px;" id="lead_nome_s"><span style="width:300px;">meu nome é: </span><input type="" class="input_c" id="lead_nome" style="margin-top:10px;" placeholder="nome" required></p>');
      $('.login_form').append('<p style="font-size:25px;" id="lead_fone_s">e para falar comigo <input type="" class="input_c" style="margin-top:20px;" id="lead_fone" placeholder="(XX) XXXXX-XXXX" required></p>');
      $('#lead_fone').mask('(00) 00000-0000');
      $('.login_form').append('<p style="font-size:25px;" id="lead_email_s">ou envie um email: <input type="" class="input_c" id="lead_email" style="margin-top:20px;" placeholder="email@email.com" style="width:300px" required></p>');
      $('.login_form').append('<a style="margin-top:20px;" href="#" id="saveLead" class="button-rounded button-rounded__orange bt-quotation">Registrar Interesse</a>')
    }
  };

  var verificaButton = function () {
    if (
      $('#lead_nome').length !== 0 &&
      $("#lead_fone").length !== 0 &&
      $('#lead_email').length !== 0 &&
      $('#lead_fone').val().length == 15 &&
      $('#cpf').val().length == 11 &&
      $('#lead_email').val().length >= 8 &&
      $('#lead_nome').val().length >= 3
    ) {
      let objectSave = {
        'lead_tag': 'form_register_interest',
        'lead_nome': $('#lead_nome').val(),
        'lead_cpf': $('#cpf').val(),
        'lead_email': $('#lead_email').val(),
        'lead_fone': $('#lead_fone').val().replace("(", "").replace(")", "").replace("-", "").replace(" ", "")
      };
      saveLead(objectSave).then(() => {
          window.location.href = "http://agentebrasil.com/lp/obrigado.html";
        },
        () => {
          window.location.href = "http://agentebrasil.com/lp/obrigado.html";
        });
    } else {
      alert('preencha todos os campos');
    }
  }

  var saveLead = function (object) {
    return Ember.$.ajax({
      'method': 'post',
      'url': 'https://client.agentebrasil.com/client/lead/store',
      data: object
    });
  }

}
export default Ember.Helper.helper(loginform);
