import Ember from 'ember';

export default Ember.Service.extend({
    ajax: Ember.inject.service(),
    h1: 'http://client.agentebrasil.com/client/',
    getMethod: function(path){
        return this.get('ajax').request(this.h1+path);
    },
    postMethod: function(path,data){
        return this.get('ajax').request(this.h1+path,{
            method: 'POST',
            data: data
        });
    },
});