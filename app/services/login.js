import Ember from 'ember';

export default Ember.Service.extend({
    requester: Ember.inject.service(), 
    getDataByCpf: function(cpf){
        return this.get('requester').getMethod('financeiro-contratacao-by-cpf/'+cpf);
    }
});